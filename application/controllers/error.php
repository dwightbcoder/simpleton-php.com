<?php
class errorController
{
	function index( $options = array() )
	{
		$exception = NULL;
		extract( $options, EXTR_IF_EXISTS );
		if ( ! $exception )
		{
			$exception = new Exception( 'Unknown error' );
		}
		
		if ( S::ajax() )
		{
			header( 'Content-Type: application/json' );
			echo json_encode(
			array(
				'status' => 'ERROR',
				'error'  => array( 'message' => $exception->getMessage() )
			));
		}
		else
		{
			if ( headers_sent() )
			{
				include S::view( 'error' );
			}
			else
			{
				$view = 'error';
				include S::layout( 'default' );
			}
		}
	}
}