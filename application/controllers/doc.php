<?php
class docController
{
	var $layout = 'default';
	
	public function index( $options = array() )
	{
		$view = 'doc/en/Simpleton/index';
		include S::layout( 'default' );
	}
	
	public function Simpleton( $options = array() )
	{
		$class = key( $options );
		$method = $options[$class];
		$method = $method ? $method : S::option('controller_default');
		
		$view = "/doc/en/Simpleton/{$class}/{$method}";
		include S::layout( $this->layout );
	}
}