<?php
include __DIR__ . '/libraries/Simpleton/System.php';

// Database
//ModelMapper::pdo( array('DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS') );

// Events
S::on( 'System.Exception', function( $args )
{
	list( $e, $fatal ) = $args;
	error_log( 'Exception = ' . $e->getMessage() );
});

S::on( 'System.404', function( $args )
{
	list( $controller, $method ) = $args;
	error_log( '404 = ' . $controller . '/' . $method );
});
