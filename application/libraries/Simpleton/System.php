<?php
namespace Simpleton;

class System
{
	private static $_options = array(
		'autoload'            => TRUE,
		'autoload_controller' => TRUE,
		'autoload_model'      => TRUE,
		'autoload_library'    => TRUE,
		
		'separator_path'       => '_',
		'separator_controller' => '-',

		'request_update'     => TRUE,
		'method_default'     => 'index',
		'controller_default' => 'index',
		'controller_error'   => 'error',
		
		'handle_fatal'     => FALSE,
		'handle_exception' => TRUE,
		'handle_error'     => TRUE,
		
		'path'               => '',
		'folder_model'       => 'models',
		'folder_view'        => 'views',
		'folder_controller'  => 'controllers',
		'folder_layout'      => 'layouts',
		'folder_library'     => 'libraries',
		
		'extension_model'       => 'php',
		'extension_view'        => 'phtml',
		'extension_controller'  => 'php',
		'extension_layout'      => 'phtml',
		'extension_library'     => 'php'
	);
	private static $_ajax   = NULL;
	private static $_events = array();
	
	public static function bootstrap()
	{
		// Execute route
		return self::route(
			isset($_GET['route']) ?
			$_GET['route'] :
			self::option('controller_default')
		);
	}
	
	public static function option()
	{
		$num_args = func_num_args();
		
		switch ( $num_args )
		{
			case 2:				
				$option = func_get_arg( 0 );
				$value  = func_get_arg( 1 );
				
				if ( isset(self::$_options[$option]) && is_array(self::$_options[$option]) )
				{
					self::$_options[$option] = array_merge( self::$_options[$option], $value );
				}
				else
				{
					self::$_options[$option] = $value;
				}
				break;
			
			case 1:
				$option = func_get_arg( 0 );
				if ( is_array($option) )
				{
					self::$_options = array_merge( self::$_options, $option );
				}
				else
				{
					return isset(self::$_options[$option]) ? self::$_options[$option] : NULL;
				}
				break;
			
			default:
				return self::$_options;
				break;
		}
	}
	
	public static function noCache()
	{
		header('Expires: -1'); // Proxies.		
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: no-cache, no-store, must-revalidate, max-age=0'); // HTTP 1.1.
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache'); // HTTP 1.0.
		header('Connection: close');
	}
	
	/**
	 * Get/set if this is an AJAX request
	 */
	public static function ajax( $ajax = NULL )
	{
		if ( $ajax !== NULL )
		{
			self::$_ajax = $ajax;
		}
		elseif ( self::$_ajax === NULL )
		{
			if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
			{
				self::$_ajax = TRUE;
			}
			elseif ( isset($GLOBALS['ajax']) )
			{
				self::$_ajax = $GLOBALS['ajax'];
			}
			elseif ( isset($_REQUEST['ajax']) )
			{
				self::$_ajax = $_REQUEST['ajax'];
			}
			else
			{
				self::$_ajax = FALSE;
			}
		}
		
		return self::$_ajax;
	}

	public static function route( $request = NULL, array $data = array() )
	{
		$request    = $request ? $request : self::option('controller_default');
		$parts      = explode( '/', $request );
		$controller = $parts[0];
		$method     = '';
		
		if ( isset($parts[1]) )
		{
			$method = $parts[1];
			
			if ( isset($parts[2]) )
			{
				$_data = array_slice( $parts, 2 );
				
				for ( $i = 0; $i < count($_data); $i += 2 )
				{
					if ( $_data[$i] )
					{
						$value = NULL;
						if ( isset($_data[$i + 1]) )
						{
							$value = $_data[$i + 1];
						}
						
						$data[$_data[$i]] = $value;
					}
				}
			}
		}
		
		if ( ! $method )
		{
			$method = self::option('method_default');
		}
		
		if ( self::option('request_update') )
		{
			$_REQUEST['route']      = $controller . '/' . $method;
			$_REQUEST['controller'] = $controller;
			$_REQUEST['method']     = $method;
		}
		
		return self::call( $controller, $method, $data );
	}
	
	public static function call( $controller, $method, array $data = array() )
	{
		if ( $controller == 'favicon.ico' )
		{
			return FALSE;
		}
		
		try
		{
			$controller = str_replace( self::option('separator_controller'), self::option('separator_path'), $controller );
			$controllerClass = "{$controller}Controller";
			
			if ( ! class_exists($controllerClass) )
			{
				$include_file = self::controller( $controllerClass );
				if ( file_exists($include_file) )
				{
					include $include_file;
				}
			}
			
			$C = new $controllerClass();
			
			if ( ! method_exists($C, $method) )
			{
				throw new \Exception( 'Method does not exist: ' . $method );
			}
			
			return $C->$method( $data );
		}
		catch( \Exception $e )
		{
			self::trigger( 'System.404', array($controller, $method, $data) );
			throw new \Exception( 'Page not found: ' . $controller . '/' . $method );
		}	
	}
	
	public static function autoload( $class )
	{
		if ( ! self::option('autoload') )
		{
			return;
		}
		
		$include_file = '';
		
		if ( ! $include_file && self::option('autoload_controller') && substr($class, -10) == 'Controller' )
		{
			$include_file = self::controller( $class );
			$include_file = file_exists($include_file) ? $include_file : '';
		}
		
		if ( ! $include_file && self::option('autoload_model') )
		{
			$include_file = self::model( $class );
			$include_file = file_exists($include_file) ? $include_file : '';
		}
		
		if ( ! $include_file && self::option('autoload_library') )
		{
			$include_file = self::library( $class );
			$include_file = file_exists($include_file) ? $include_file : '';
		}
		
		if ( $include_file && file_exists($include_file) )
		{
			include $include_file;
		}
		else
		{
			throw new \Exception( 'Could not autoload ' . $class );	
		}
	}
	
	
	// ! Event system
	
	/**
	 * Add an event callback
	 */
	public static function on( $event, $callback )
	{
		if ( is_callable($callback) )
		{
			if ( ! isset(self::$_events[$event]) )
			{
				self::$_events[$event] = array();
			}
			
			self::$_events[$event][] = $callback;
			
			return TRUE;
		}
		
		throw new \Exception( 'Callback is not callable: ' . (string)$callback );	
		
		return FALSE;
	}

	/**
	 * Remove an event callback
	 */	
	public static function off( $event, $callback )
	{
		if ( $callback === FALSE )
		{
			unset( self::$_events[$event] );
			return TRUE;
		}
		else
		{
			foreach( self::$_events[$event] as $i => $_callback )
			{
				if ( $_callback === $callback )
				{
					unset( self::$_events[$event][$i] );
					return TRUE;
				}
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Trigger all registered event callbacks
	 */
	public static function trigger( $event, array $arguments = array() )
	{
		if ( isset(self::$_events[$event]) )
		{
	        foreach( self::$_events[$event] as $_callback )
	        {
				$return = call_user_func_array( $_callback, array(&$arguments) );
				if ( $return === FALSE )
				{
					break;
				}
	        }
	        
	        return $arguments;
		}
		
		return $arguments;
	}
	
	
	// ! Path shorthands
	
	public static function path( $basename )
	{
		if ( substr($basename, -10) == 'Controller' )
		{
			$basename = substr( $basename, 0, strlen($basename) - 10 );
			$basename = str_replace( self::option('separator_controller'), self::option('separator_path'), $basename);
		}
		
		$basename = str_replace( array('\\', self::option('separator_path')), DIRECTORY_SEPARATOR, $basename );
		return $basename;
	}
	
	public static function model( $model )
	{
		return self::option('path') . '/' . self::option('folder_model') . '/' . self::path($model) . '.' . self::option('extension_model');
	}
	
	public static function view( $view )
	{
		return self::option('path') . '/' . self::option('folder_view') . '/' . self::path($view) . '.' . self::option('extension_view');
	}
	
	public static function controller( $controller )
	{
		return self::option('path') . '/' . self::option('folder_controller') . '/' . self::path($controller) . '.' . self::option('extension_controller');
	}
	
	public static function layout( $layout )
	{
		return self::option('path') . '/' . self::option('folder_layout') . '/' . self::path($layout) . '.' . self::option('extension_layout');
	}
	
	public static function library( $library )
	{
		return self::option('path') . '/' . self::option('folder_library') . '/' . self::path($library) . '.' . self::option('extension_library');
	}
	
	
	// ! Error handling
	
	public static function handleError( $errno, $errstr, $errfile, $errline )
	{
	    if ( self::option('handle_fatal') || ! self::option('handle_error') || ! (error_reporting() & $errno) )
	    {
	        return FALSE;
	    }
		
		throw new \Exception( $errstr, $errno );
		
		return TRUE;
	}
	
	public static function handleException( $e, $fatal = FALSE )
	{
		self::trigger( 'System.Exception', array($e, $fatal) );
		
		if ( self::option('handle_exception') || ($fatal && self::option('handle_fatal')) )
		{
			self::call( self::option('controller_error'), self::option('method_default'), array('exception' => $e) );
		}
	}
	
	public static function handleShutdown()
	{
		if ( ! self::option('handle_fatal') )
		{
			return;
		}
		
		$error = error_get_last();
		if ( $error )
		{
			$e = new \Exception(
				$error['message'],
				$error['type']
			);
	
			self::handleException( $e, TRUE );
		}
	}

}

System::option( 'path', realpath(__DIR__ . '/../..') );

// Setup error/exception handling
register_shutdown_function( 'Simpleton\System::handleShutdown' );
set_exception_handler( 'Simpleton\System::handleException' );
set_error_handler( 'Simpleton\System::handleError' );
// Auto loader
spl_autoload_register( 'Simpleton\System::autoload' );

try
{
	class_alias( 'Simpleton\System', 'Simpleton' );
	class_alias( 'Simpleton\System', 'S' );
}
catch( \Exception $e ){}
