<?php
namespace Simpleton;

class ModelMapper
{
	public static $PDO = NULL;
	protected static $Model      = 'Model';
	protected static $TableName  = '';
	protected static $PrimaryKey = 'id';
	protected static $Columns    = array();
	protected static $ColumnsEncrypted = array();
	protected static $_paramTypes = array(
		'bool'     => PDO::PARAM_BOOL,
		'null'     => PDO::PARAM_NULL,
		'int'      => PDO::PARAM_INT,
		'string'   => PDO::PARAM_STR,
		'chunk'    => PDO::PARAM_LOB,
		'date'     => PDO::PARAM_STR,
		'time'     => PDO::PARAM_STR,
		'datetime' => PDO::PARAM_STR
	);

	protected static $ENCRYPTION_CYPHER = MCRYPT_RIJNDAEL_256;
	protected static $ENCRYPTION_MODE   = MCRYPT_MODE_ECB;
	protected static $ENCRYPTION_SECRET = NULL;	
	protected static $ENCRYPTION_KEY    = NULL;
	protected static $ENCRYPTION_IV     = NULL;
	
	public static function &pdo( $options = array() )
	{
		list( $host, $name, $user, $pass ) = $options;
		
		if ( self::$PDO )
		{
			return self::$PDO;
		}
	
		return self::$PDO = new PDO( "mysql:host={$host};dbname={$name}", $user, $pass );
	}
	
	/**
	 * Public read-only access to $Model
	 */
	public static function model()
	{
		return static::$Model;
	}

	/**
	 * Public read-only access to $TableName
	 */	
	public static function table()
	{
		return static::$TableName;
	}

	/**
	 * Public read-only access to $PrimaryKey
	 */
	public static function primaryKey()
	{
		return static::$PrimaryKey;
	}
	
	/**
	 * Extra just the column mapping data from the ::$Columns config
	 */
	public static function columnMap()
	{
		$map = array();
	
		foreach ( static::$Columns as $_column => $_config )
		{
			if ( isset($_config['map']) )
			{
				$map[$_column] = $_config['map'];
			}
		}
		
		return $map;
	}
	
	public static function columnType( $column )
	{
		$type = PDO::PARAM_STR;
		
		if ( isset(static::$Columns[$column]) && isset(static::$Columns[$column]['type']) && isset(self::$_paramTypes[static::$Columns[$column]['type']]) )
		{
			$type = self::$_paramTypes[static::$Columns[$column]['type']];
		}

		return $type;
	}
	
	public static function columns()
	{
		return static::$Columns;
	}
	
	public static function values( $rows, $field )
	{
		$values = array();
		
		foreach ( $rows as &$_row )
		{
			$values[] = $_row->{$field}();
		}
		
		return $values;
	}
	
	public static function fetch( $id, $where = '1=1', $sort = 'DESC', $binds = array() )
	{
		return static::fetchBy( static::$PrimaryKey, $id, $where, $sort, $binds );
	}
	
	public static function fetchBy( $column, $value, $where = '1=1', $sort = 'DESC', $binds = array() )
	{
		$map    = array_flip( static::columnMap() );
		$column = $map[$column] ? $map[$column] : $column;
		$binds  = array_merge( $binds, array(':'.$column => $value) );
		
		return static::query(
			sprintf( "SELECT * FROM `%s` WHERE (`%s` = :%s) AND (%s) ORDER BY `%s` %s LIMIT 1;", static::$TableName, $column, $column, $where, $column, $sort ),
			$binds
		);
	}
	
	public static function fetchAllBy( $column, $value, $where = '1=1', $sort = 'DESC', $binds = array() )
	{
		$map    = array_flip( static::columnMap() );
		$column = $map[$column] ? $map[$column] : $column;
		$whereColIsValue = '';
		
		if ( is_array($value) )
		{
			$whereColIsValue = array();
			foreach ( $value as $i => $_value )
			{
				$binds[':' . $column . $i] = $_value;
				$whereColIsValue[] = '`' . $column . '` = :' . $column . $i;
			}
			$whereColIsValue = implode( ' OR ', $whereColIsValue );
			
			if ( ! $whereColIsValue )
			{
				$whereColIsValue = '1=0';
			}
		}
		else
		{
			$binds[':' . $column] = $value;
			$whereColIsValue = '`' . $column . '` = :' . $column;
		}
		
		return static::query(
			sprintf( "SELECT * FROM `%s` WHERE (%s) AND (%s) ORDER BY `%s` %s;", static::$TableName, $whereColIsValue, $where, $column, $sort ),
			$binds
		);
	}
	
	public static function fetchAll( $where = '1=1', $sort = 'DESC', $binds = FALSE, $orderBy = FALSE )
	{
		$orderBy = $orderBy === FALSE ? '`' . static::$PrimaryKey . '`' : $orderBy;
		return static::query( sprintf("SELECT * FROM `%s` WHERE (%s) ORDER BY %s %s;", static::$TableName, $where, $orderBy, $sort), $binds );
	}
		
	public static function query( $sql, $binds = FALSE )
	{
		$limit = strpos( strtolower($sql), 'limit 1' ) !== FALSE;
		$stmt = self::$PDO->prepare( $sql );
		
		if ( $binds && is_array($binds) )
		{
			foreach ( $binds as $_param => $_value )
			{
				$_did_bind = $stmt->bindValue( $_param, $_value, static::columnType($_param) );
			}
		}
		
		if ( $stmt->execute() )
		{
			if ( ! $limit )
			{
				$rows = array();
			}
			
			while ( $row = $stmt->fetch(PDO::FETCH_ASSOC) )
			{
				$_data = static::map( $row );
				
				if ( static::$ColumnsEncrypted )
				{
					$_data = static::decrypt( $_data );
				}
				
				if ( $limit )
				{
					return new static::$Model( $_data );
				}
				else
				{
					$rows[] = new static::$Model( $_data );
				}
			}
			
			if ( ! $limit )
			{
				return $rows;
			}
		}
		else
		{
			$errorInfo = $stmt->errorInfo();
			throw new \Exception( utf8_encode($errorInfo[2]), (int)$stmt->errorCode(), NULL );
		}
		
		return FALSE;
	}
	
	public static function execute( $sql, $binds = FALSE )
	{
		$stmt = self::$PDO->prepare( $sql );
		
		if ( $binds && is_array($binds) )
		{
			foreach ( $binds as $_param => $_value )
			{
				$_did_bind = $stmt->bindValue( $_param, $_value, static::columnType($_param) );
			}
		}
		
		if ( ! $stmt->execute() )
		{
			$errorInfo = $stmt->errorInfo();
			throw new \Exception( utf8_encode($errorInfo[2]), (int)$stmt->errorCode(), NULL );
		}
		
		return $stmt;
	}
	
	/**
	 * Rename the data keys based on the set FieldMap
	 * Use $flip=TRUE to reverse the mapping
	 */
	public static function map( $row, $flip = FALSE )
	{
		$map = static::columnMap();
		
		if ( ! empty($map) && ! empty($row) )
		{
			if ( $flip )
			{
				$map = array_flip( $map );
			}
			
			$data = array();
		
			foreach ( $row as $_key => $_value )
			{
				if ( isset($map[$_key]) )
				{
					$_key = $map[$_key];
				}

				$data[$_key] = $_value;
			}
			
			return $data;
		}
		
		return $row;
	}
	
	/**
	 * Retrieve a models data mapped to the pre-defined database fields
	 */
	public static function data( $model )
	{
		if ( is_array($model) )
		{
			$data = array();
			
			foreach ( $model as $_model )
			{
				if ( static::$ColumnsEncrypted )
				{
					$data[] = static::encrypt( self::data($_model) );
				}
				else
				{
					$data[] = self::data( $_model );
				}
			}
			
			return $data;
		}
		
		$data = $model->toArray();
		
		if ( static::$ColumnsEncrypted )
		{
			$data = static::encrypt( $data );
		}
		
		$data = static::map( $data, TRUE );
		
		if ( static::$Columns )
		{
			// Only allow pre-defined fields 
			$keys = array_keys( $data );
			foreach ( $keys as $_key )
			{
				if ( ! isset(static::$Columns[$_key]) )
				{
					unset( $data[$_key] );
				}
			}
		}
		
		return $data;
	}
	
	public static function toArray( $model, $deep = TRUE )
	{
		$data = array();		
		
		if ( is_array($model) )
		{
			foreach ( $model as $_model )
			{
				$data[] = $_model->toArray( $deep );
			}
		}
		else
		{
			$data[] = $model->toArray( $deep );
		}
		
		return $data;
	}
	
	/**
	 * Group rows by column key, if none is specified use Primary Key
	 */
	public static function rowsGroupByKey( &$rows, $key = FALSE, $field = FALSE )
	{
		$grouped = array();
		
		if ( ! $key )
		{
			$key = static::$PrimaryKey;
		}
		
		foreach ( $rows as &$_row )
		{
			$_key = $_row->{$key}();
		
			if ( ! isset($grouped[$_key]) )
			{
				$grouped[$_key] = array();
			}
		
			if ( $field )
			{
				$grouped[$_key][] = $_row->{$field}();
			}
			else
			{
				$grouped[$_key][] =& $_row;
			}
		}
		
		return $grouped;
	}
	
	public static function &rowsToKeyIndexes( &$rows, $key = FALSE )
	{
		if ( ! $rows || ! is_array($rows) )
		{
			return $rows;
		}
		
		$_rows = array();
		
		if ( ! $key )
		{
			$key = static::$PrimaryKey;
		}
		
		foreach ( $rows as &$_row )
		{
			$_key = $_row->{$key}();
			$_rows[$_key] =& $_row;
		}
		
		$rows = $_rows;
	}
	
	/**
	 * Save the model's data to the database
	 * Automatically choose UPDATE/INSERT based on if the PrimaryKey is set
	 * @return FALSE on fail, last insert id on INSERT, row count on UPDATE
	 * Note: Be prepared to have a return result of 0 when no rows are affect (no data has changed)
	 * Check for boolean === FALSE for a failed save
	 */
	public static function save( &$model )
	{
		$data   = static::data( $model );
		$result = FALSE;
		$update = isset($data[static::$PrimaryKey]) && $data[static::$PrimaryKey];

		if ( $update )
		{
			// Update
			$sql = static::updateQuery( $data );
		}
		else
		{
			// Insert
			//$sql = static::insertQuery( $data );
			$sql = static::insertUpdateQuery( $data );
		}
		
		$stmt = self::$PDO->prepare( $sql );
		
		foreach ( $data as $_param => $_value )
		{
			if ( $_value === '' )
			{
				$_value = NULL;
			}
			
			$stmt->bindValue( $_param, $_value, static::columnType($_param) );
		}
		
		if ( $stmt->execute() )
		{
			if ( $update )
			{
				$result = $stmt->rowCount();
			}
			else
			{
				$result = self::$PDO->lastInsertId();
				$model->id( $result );
			}
		}
		else
		{
			$errorInfo = $stmt->errorInfo();
			throw new \Exception( $errorInfo[2], $stmt->errorCode() );
		}
		
		return $result;
	}
	
	public static function delete( $model )
	{
		$stmt = static::execute(
			sprintf( "DELETE FROM `%s` where `%s` = :id LIMIT 1;", static::$TableName, static::$PrimaryKey ),
			array(
				':id' => $model->{static::$PrimaryKey}()
			));
		
		$errorCode = $stmt->errorCode();
		return ($errorCode && $errorCode != '00000') ? FALSE : TRUE;
	}
	
	public static function count( $where = '1=1', $binds = array() )
	{
		$stmt = static::execute(
			sprintf( "SELECT COUNT(`%s`) as `count` FROM `%s` WHERE (%s);", static::$PrimaryKey, static::$TableName, $where ),
			$binds
		);
		
		$count = $stmt->fetch();
		return (int)$count[0];
	}
	
	/**
	 * Generate an UPDATE SQL query
	 */
	public static function updateQuery( $data, $where = '' )
	{
		$params = array();
		foreach ( array_keys($data) as $_key )
		{
			$params[] = '`' . $_key . '` = :' . $_key;
		}
		
		if ( ! $where )
		{
			$where = '`' . static::$PrimaryKey . '` = :' . static::$PrimaryKey;
		}
	
		return sprintf(
			"UPDATE %s SET %s WHERE %s;",
			static::$TableName,
			implode(', ', $params),
			$where
		);
	}
	
	/**
	 * Generate an INSERT SQL query
	 */
	public static function insertQuery( $data )
	{
		return sprintf(
			"INSERT INTO %s (`%s`) VALUES (%s);",
			static::$TableName,
			implode( '`, `', array_keys($data) ),
			':' . implode(', :', array_keys($data))
		);
	}
	
	/**
	 * Generate an INSERT ON DUPLICATE KEY UPDATE SQL query
	 */
	public static function insertUpdateQuery( $data )
	{
		$columns = array_keys($data);
		$update = array();
		foreach ( $columns as $_column )
		{
			$update[] = '`' . $_column . '` = :' . $_column;
		}
		
		$sql = sprintf(
			"INSERT INTO %s (`%s`) VALUES (%s) ON DUPLICATE KEY UPDATE %s, `%s`=LAST_INSERT_ID(`%s`);",
			static::$TableName,
			implode( '`, `', $columns ),
			':' . implode(', :', $columns),
			implode( ', ', $update ),
			static::$PrimaryKey,
			static::$PrimaryKey
		);
		
		return $sql;
	}
	
	/**
	 * Handle SSP data for DataTables (SpryMedia)
	 * @dependency SpryMedia SSP library
	 */
	public static function ssp( $request, $columns = array(), $and_where = '' )
	{
		include_once \Simpleton\System::library( 'sprymedia/datatables/ssp.class' );
		
		$dtColumns = array();
		
		if ( empty($columns) )
		{
			$columns = array_keys( static::$Columns );
		}
			
		foreach ( $columns as $_column )
		{
			$dt = $_column;
			if ( isset(static::$Columns[$_column]['map']) )
			{
				$dt = static::$Columns[$_column]['map'];
			}
			
			$dtColumns[] = array( 'db' => $_column, 'dt' => $dt );
		}
		
		$table      = static::$TableName;
		$primaryKey = static::$PrimaryKey;
		$bindings   = array();

		// Build the SQL query string from the request
		$limit = SSP::limit( $request, $dtColumns );
		$order = SSP::order( $request, $dtColumns );
		$where = SSP::filter( $request, $dtColumns, $bindings );
		
		if ( $and_where )
		{
			if ( $where )
			{
				$where .= ' AND (' . $and_where . ')';
			}
			else
			{
				$where = 'WHERE (' . $and_where . ')';
			}
		}

		// Main query to actually get the data
		$data = SSP::sql_exec( self::$PDO, $bindings,
			"SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", SSP::pluck($dtColumns, 'db'))."`
			 FROM `$table`
			 $where
			 $order
			 $limit"
		);

		// Data set length after filtering
		$resFilterLength = SSP::sql_exec( self::$PDO,
			"SELECT FOUND_ROWS()"
		);
		$recordsFiltered = $resFilterLength[0][0];

		// Total data set length
		$resTotalLength = SSP::sql_exec( self::$PDO,
			"SELECT COUNT(`{$primaryKey}`)
			 FROM   `$table`"
		);
		$recordsTotal = $resTotalLength[0][0];
		
		$data = SSP::data_output( $dtColumns, $data );
		
		// Add DataTable row ids
		foreach ( $data as &$_row )
		{
			$_row['DT_RowId'] = static::$Model . '_row_' . $_row[static::$PrimaryKey];
		}
		
		/*
		 * Output
		 */
		return array(
			'draw'            => intval( $request['draw'] ),
			'recordsTotal'    => intval( $recordsTotal ),
			'recordsFiltered' => intval( $recordsFiltered ),
			'data'            => $data
		);
	}
	
	// ! ENCRYPTION
	public static function encryption_secret( $secret = NULL )
	{
		if ( $secret )
		{
			self::$ENCRYPTION_SECRET = $secret;
		}
		
		if ( ! self::$ENCRYPTION_SECRET )
		{
			throw new \Exception( 'Secret key not specified' );
		}
		
		return self::$ENCRYPTION_SECRET;
	}
	
	public static function encryption_key()
	{
		if ( ! self::$ENCRYPTION_KEY )
		{
			self::$ENCRYPTION_KEY = hash( 'sha256', self::encryption_secret(), TRUE );
		}
		
		return self::$ENCRYPTION_KEY;
	}
	
	public static function encryption_iv()
	{
		return mcrypt_create_iv( 32, MCRYPT_DEV_URANDOM );
		
		if ( ! self::$ENCRYPTION_IV )
		{
			self::$ENCRYPTION_IV = mcrypt_create_iv( 32, MCRYPT_DEV_URANDOM );
		}
		
		return self::$ENCRYPTION_IV;
	}
	
	public static function encrypt( $data )
	{
		if ( is_array($data) )
		{
			// Encrypt specified columns
			foreach ( static::$ColumnsEncrypted as $_column )
			{
				if ( isset($data[$_column]) )
				{
					$data[$_column] = static::encrypt( $data[$_column] );
				}
			}
		}
		else
		{
			$data = base64_encode(mcrypt_encrypt(
				self::$ENCRYPTION_CYPHER,
				self::encryption_key(),
				$data,
				self::$ENCRYPTION_MODE,
				self::encryption_iv()
			));
		}
		
		return $data;
	}
	
	public function decrypt( $data )
	{
		if ( is_array($data) )
		{
			// Encrypt specified columns
			foreach ( static::$ColumnsEncrypted as $_column )
			{
				if ( isset($data[$_column]) )
				{
					$data[$_column] = static::decrypt( $data[$_column] );
				}
			}
		}
		else
		{
			$data = trim(mcrypt_decrypt(
				self::$ENCRYPTION_CYPHER,
				self::encryption_key(),
				base64_decode($data),
				self::$ENCRYPTION_MODE,
				self::encryption_iv())
			);
		}
		
		return $data;
	}
}
